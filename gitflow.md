# Git

Here are our rules for branches/commits/pull-requests with Git.

It's very important to follow because it affects work of whole team, deployment and ci workflows. 

## Definitions

* `taskId` - ID of task from task tracker.
For JIRA it is two capital letter (that represent current project) and numbers.
Example: `FR-101`.

* `subject` - a succinct description of the change in imperative, present tense: "change" not "changed" nor "changes". Example: `add google authentication`.



# Branches Naming Format

We use following format for naming new branches in repository:

```
<taskId>-<subject>
```

Where `subject` is in lower case with words joined with `-`. Example: `add-google-authentication`.

Example of branch name: 

```
FR-101-add-google-authentication
```


# Commit Message Format

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
in future we will use the git commit messages to **generate change log**.

Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope**, a **taskId** and a **subject**:

```
<type>(<scope>)<taskId>: <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory, **body** and **footer** are optional.

Any line of the commit message cannot be longer 80 characters! This allows the message to be easier
to read on GitLab/GitHub/BitBucket as well as in various git tools.

The footer should contain a closing reference to an issue [GitLab](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html)/[GitHub](https://help.github.com/articles/closing-issues-via-commit-messages/)/[BitBucket](https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html) if any.

Samples: (even more [samples](https://gitlab.com/anton-shubin-and-partners/style-guides-and-best-practices/commits/master))

```
fix(angular/eatery/protected/orders)FR-1017: replace icons with suitable ones
```
```
feat(firebase/functions/fs/users)FR-1354: use firebase transactions in onUpdate

Using transactions in this case makes DB data consistent.
```

### Type
Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **environment**: Changes in build system, dev-scripts, gitignore, package.json, etc
* **docs**: Documentation only changes
* **refactor**: A code change that neither fixes a bug nor adds a feature, but makes code cleaner
* **style**: Changes to visual part of the application only. Doesn't change logic.
* **test**: Adding missing tests or correcting existing tests

### Scope
The scope should be the name of the app module affected (as perceived by the person reading the changelog generated from commit messages.

Examples:

* **angular/eatery/onboarding**
* **angular/eatery/protected/orders**
* **angular/shared/style**
* **firebase/functions/fs/orders**
* **firebase/functions/https/orders**
* **firebase/firestore/rules/orders**
* **website/blog**
* **microservices/sql-mirror/products**
* **dev-scripts/build**

### Subject
The subject contains a succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to
reference GitLab/GitHub/BitBucket issues that this commit **Closes**/**Relates**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.

Example (imagine we have public Angular module with components):
```
feat(components/componentA): simplify inputs names

Make some inputs names shorter and cleaner

BREAKING CHANGE: Some inputs changed their names.

Before:

<asap-organization [organizationId]="x" [organizationTitleWhileLoading]="y"></asap-organization>

After:

<asap-organization [id]="x" [title]="y"></asap-organization>
```
