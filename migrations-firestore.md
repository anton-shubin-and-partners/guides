###Rules for writing migration using best practices and our standards:

1. Migration must use our migration helpers. Don't ignore them. If helpers can be used - improve helpers.
1. Migrations must provide correct logs to watch the progress. If we can't see progress during 10sec - migration is written wrong.
3. It's good if items performing sequential - it allows us to see correct logs, but we can performing items parallel 
if we need to speed up the migration.
4. Migrations must be performing groups of items (in our case items that belong to one organization) sequential and 
must have the ability to filter items by the organization. Flag `-o ORG1,ORG2`.
5. Migration must perform items from the main collection by batches. Default batch size is 500. Items from related 
collections must be loaded by `firestore.getAll()` after bunch from the main collection loaded.
    
    5.1. Loading - whole collections - it's wrong.
    
    5.2. Loading - items one by one and then loading related items for each one - it's wrong.
6. Migration must always use DatabaseService and have two modes: read/analyze (default) and write/fixing (flag `-w`)
7. Migration must have a strict condition to check "is document incorrect and need fixing". We must not update any 
document if it already correct (See point 10 about exceptions).
8. Migration must fix all document by one starting. Second starting of migration must display result "All documents 
are correct".
9. Some migration can have "fast mode" for fast analyze problems and "detailed mode" for carefully search rare 
problems spending more time. Flag `-d` (detailed). Adding this mode to any migration needs discussing.
10. Some migration can have "berserk mode" - for fast fixing problems without any checking. Flag `-b`. Adding this 
mode to any migration needs discussing.
11. Migration must be detailed documented (using JSDocs)
  
    11.1. Developer must be detailed describe what migration doing
    
    11.2. Developer must describe what error or situation may be the reason of a problem when we need this migration
    
    11.3. If migration using any other keys except firebase-account-key then must be described path to these keys
    
    11.4. If the running of migration leads to starting any Firebase Functions triggering - it must be described in 
  JSDocs.
12. Keys and configurations must be loaded automatically by specifying the environment. Flag 
`--env=[prod|qa|e2e"dev]` or it's short form `--[prod|qa|e2e"dev]`
13. Migration must perform backup data that will be changed during performing if Write mode turned on.
14. If we can use blocks of code from the application to perform data in migration - we must use it to avoid code 
duplication and be sure that we performing data in the same way.

Also, need separate migrations for two folders: 
1. for long-term migration that we can relaunch regularly  and 
2. for single time migration that fixes one particular case for any problem.


